const express = require(`express`)
const app = express()
const PORT = process.env.PORT || 8000

console.log(`Database Url: `, process.env.DATABASE_URL)

app.use(express.json())

app.get(`/`, (req, res) => {
    res.status(200).json({
        status: `success`,
        message: `Hello Mars`
    })
})

app.listen(PORT, () => {
    console.log(`Server started at ${Date()}`)
    console.log(`Listening on port ${PORT}`)
})